terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.55.0"
    }
  }
}

provider "aws" {
  region = var.region
  default_tags {
    tags = {
      Name        = "${var.project_name}_${terraform.workspace}"
      Environment = terraform.workspace
    }
  }
}

