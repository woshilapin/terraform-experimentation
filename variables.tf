variable "region" {
  type    = string
  default = "eu-west-1"
}

variable "project_name" {
  type        = string
  description = "Name of the project, usually a famous psy."

}

variable "multi_region_enabled" {
  type        = bool
  default     = false
  description = "Should the stack be deployed in a multi-region stack? (default: false)"
}

variable "cidr_block" {
  type        = string
  description = "CIDR block for network"
  validation {
    condition     = can(regex("^([0-9]{1,3}\\.){3}[0-9]{1,3}(/(16|24))?$", var.cidr_block))
    error_message = "CIDR block is not valid; it should look like '123.123.123.123/16'."
  }
}

locals {
  az_number     = length(data.aws_availability_zones.available.names)
  private_cidrs = [for idx in range(local.az_number) : cidrsubnet(var.cidr_block, 8, idx)]
  public_cidrs  = [for idx in range(local.az_number, 2 * local.az_number) : cidrsubnet(var.cidr_block, 8, idx)]
}