output "private_cidrs" {
  value = local.private_cidrs
}
output "public_cidrs" {
  value = local.public_cidrs
}

output "vpc" {
  value = module.vpc.vpc_id
}

output "private_subnets" {
  value = module.vpc.private_subnets
}

output "public_subnets" {
  value = module.vpc.public_subnets
}