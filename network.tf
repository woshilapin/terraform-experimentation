module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = ">= 3.7.0"

  name = "${var.project_name}_network_${terraform.workspace}"
  cidr = var.cidr_block

  azs             = data.aws_availability_zones.available.names
  private_subnets = local.private_cidrs
  public_subnets  = local.public_cidrs

  enable_ipv6        = false
  enable_nat_gateway = false

  public_dedicated_network_acl      = true
  private_dedicated_network_acl     = false
  elasticache_dedicated_network_acl = false
  public_inbound_acl_rules = [
    {
      rule_number = 900
      rule_action = "allow"
      from_port   = 1024
      to_port     = 65535
      protocol    = "tcp"
      cidr_block  = "10.0.0.0/16"
    }
  ]
  public_outbound_acl_rules = [
    {
      rule_number = 900
      rule_action = "allow"
      from_port   = 32768
      to_port     = 65535
      protocol    = "tcp"
      cidr_block  = "10.0.0.0/16"
    }
  ]
  private_inbound_acl_rules = [
    {
      rule_number = 900
      rule_action = "allow"
      from_port   = 1024
      to_port     = 65535
      protocol    = "tcp"
      cidr_block  = "10.0.0.0/16"
    }
  ]
  private_outbound_acl_rules = [
    {
      rule_number = 900
      rule_action = "allow"
      from_port   = 32768
      to_port     = 65535
      protocol    = "tcp"
      cidr_block  = "10.0.0.0/16"
    }
  ]
}